# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
((UKAZI GIT))
 git clone https://am2262@bitbucket.org/am2262/stroboskop.git
 cd stroboskop
 add jscolor.js
 rm nepotrebno.js
```

Naloga 6.2.3:

https://bitbucket.org/am2262/stroboskop/commits/7e102de64b1afc4c36f6bd12be5ad7d0ff791ef8

## 6.3 Skladnja strani in stilska preobrazba //Popravek naloge 6.3.4: https://bitbucket.org/am2262/stroboskop/commits/a81b2ef1f069edfd9ef30abd8adc4e7bc1684b97

Naloga 6.3.1:
 
https://bitbucket.org/am2262/stroboskop/commits/1b8c095426e58a95bcf3e711d2289cf341ac5097?at=izgled

Naloga 6.3.2:
https://bitbucket.org/am2262/stroboskop/commits/c58ab12cacfca2f2ba54427f536e9167f1700b8b

Naloga 6.3.3:
https://bitbucket.org/am2262/stroboskop/commits/4663583a012f9fc1d3adf61f5279fe4a5eee6f50

Naloga 6.3.4:
https://bitbucket.org/am2262/stroboskop/commits/05ad0749abd1843dd409484e6fbc3839165a39a0

Naloga 6.3.5:
https://bitbucket.org/am2262/stroboskop/commits/00ccf6a449186112e4a1c4b69dfe9a7982258ffc

```
((UKAZI GIT))
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/am2262/stroboskop/commits/b7adb4905ff97985a5f8146d7f3ab0546f1793a4?at=dinamika

Naloga 6.4.2:
https://bitbucket.org/am2262/stroboskop/commits/a44ef8af2ce6e7913affa3e4c7c13cb9638b47f0

Naloga 6.4.3:
https://bitbucket.org/am2262/stroboskop/commits/c990be4c678ca10201628f290f6d5a44c5c74394

Naloga 6.4.4:
https://bitbucket.org/am2262/stroboskop/commits/00ccf6a449186112e4a1c4b69dfe9a7982258ffc
